// decide whether to show 'more' button or not
function updateNav() {
  var navEl = $('#nav-el'),
      navContainer = $('#nav-container'),
      navCollapsedIndicator = $('#nav-collapsed-indicator'),
      navBtnMore = $('.secnav-btn-more');
  
  // check if elements are hidden
  if ( navCollapsedIndicator.position().top > 1) {
    navEl.addClass('overflowing');
    return false;
  } else {
    navEl.removeClass('overflowing');  
  }
}

$(window).smartresize(function() {
  updateNav();
}, 250)

// update button visibility on page load
updateNav();

// if either of the more / close buttons are clicked, toggle the nav collapsing
$("[class^='secnav-btn-']").on("click", function(event) {
  $('.secnav').toggleClass('open');
});

// add active class to elements when they are clicked
$('.secnav a').on('click', function(event) {
  // cache the clicked el
  clickedEl = $(event.target);

  // prevent anchor from taking us to a new page
  event.preventDefault();

  // check that the clicked el isn't already active
  if ( clickedEl.hasClass('active') ) {
    return false;
  }

  // remove active class from previously selected nav item
  $('.secnav a.active').removeClass('active').parent('li');
  
  // add the active class to the newly selected nav element
  clickedEl.addClass('active');

  // if the nav view is open, close it and update the view
  if ( $('#nav-el').hasClass('open') ) {
    $('#nav-el').removeClass('open');

    // if the newly selected nav element can't now be seen, then shuffle it to the beginning
    if ( clickedEl.position().top > 1 ) {
      clickedEl.parent('li').addClass('re-order');
    }
  }
});