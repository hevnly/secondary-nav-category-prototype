# How to get it up and running

* In your terminal window, `cd` your way to the repo.
* Run `php -S localhost:0987` (or whatever port you want it on)
* Make sure index.html is pointing to your local copy of Asset Bazaar
* In Asset Bazaar, checkout branch 'feature/category-module'. Be sure to run Grunt so you have the compiles LESS.
* Navigate your browser to `localhost:0987`
* Tada!
